function [response, responseTime, reactionTime, keyCode] = KbWaitForKeys(keys, waitTime)
% get response http://docs.psychtoolbox.org/KbWait
% http://wiki.stdout.org/matlabcookbook/Collecting%20data/Getting%20time-sensitive%20keyboard%20input/
% is KbWait as precise as KbCheck in a while loop?
    response = false;
    reactionTime = 0.0;
    startTime = GetSecs();
    while GetSecs() - startTime < waitTime,
        [keyIsDown, responseTime, keyCode, deltaSecs] = KbCheck;
        if keyIsDown && any(keyCode(keys)),
            reactionTime = responseTime - startTime;
            response = true;
            break;
        end
    end
end