function expdata = setup_experiment()

sca;
clear;
clc;

%**************************************************************************
%       EXPERIMENT DATA STRUCTURE
%**************************************************************************
expdata = struct();
expdata.currentDir = pwd;
expdata.dataDir = [pwd '/expdata/'];
mkdir(expdata.dataDir); 


%**************************************************************************
%       PARTICIPANT INFORMATION
%**************************************************************************
expdata.parcode = input('Participant code? ','s');
expdata.sessn = str2double(input('Session number? ', 's'));
expdata.fname = [expdata.dataDir 'expdata_' expdata.parcode '_s' ...
    num2str(expdata.sessn)];


%**************************************************************************
%       META-PARAMETERS
%**************************************************************************
expdata.train = false;
expdata.scan = false;
expdata.debug = true;


%**************************************************************************
%       EXPERIMENT PARAMETERS
%**************************************************************************
expdata.timeInstruction = 3.0;  % time to display instructions
expdata.timeDelays = [1 7 15 30 50 60 90 100 120 150 160 190];
%expdata.timeDelays = [1 7];
expdata.ntrials = length(expdata.timeDelays);

expdata.controlStartRadius = 30 * randi(10, 1);
expdata.controlIncreaseFactor = 0.5 * randi(10, 1);
if rand > 0.5
    expdata.controlAction = 'Increase';
else
    expdata.controlAction = 'Decrease';
end

expdata.showReferenceCircle = true;
expdata.referenceCircleRadius = 70;
expdata.referenceCircleDelay = 10;
expdata.timeDisplayReference = 5;

expdata.timeDisplayInstruction = 3;
expdata.timeAdjustment = 10;
expdata.timeDisplayFinalCircle = 1;
expdata.timeBetweenTrials = 1;


%**************************************************************************
%       EXPERIMENT DATA
%**************************************************************************
expdata.responseTime = cell(expdata.ntrials, 1);
expdata.reactionTime = cell(expdata.ntrials, 1);
expdata.responseType = cell(expdata.ntrials, 1);


%**************************************************************************
%       INITIALIZE PTB
%**************************************************************************
global windowPtr;
global screenCenter;
global screenSize;
screenSize = get(0,'ScreenSize');
screenCenter = [floor((screenSize(1)+screenSize(3))/2) ...
    floor((screenSize(2)+screenSize(4))/2)];
windowPtr = Screen(0, 'OpenWindow');
%windowPtr = Screen(0, 'OpenWindow',[],[1 1 640 400]);


%**************************************************************************
%       COLORS
%**************************************************************************
global colorText;
global colorBackground;
global colorCircleDefault;
global colorCircleSubmit;
white = WhiteIndex(windowPtr);
black = BlackIndex(windowPtr);
lightBlue = [77 153 255];
darkBlue = [0 77 204];
colorBackground = white;
colorText = black;
colorCircleDefault = lightBlue;
colorCircleSubmit = darkBlue;
Screen(windowPtr, 'FillRect', colorBackground);
Screen(windowPtr, 'TextColor', colorText);


%**************************************************************************
%       SHAPES
%**************************************************************************
expdata.circleIncrementSize = 2;
expdata.circleDefaultRadius = 100;
expdata.circleMinRadius = 1;
expdata.circleMaxRadius = 350;


%**************************************************************************
%       TEXT
%**************************************************************************
global textSize;
global textFont;
textSize = 30;
textFont = 'Arial';
Screen('TextFont', windowPtr, textFont);
Screen('TextSize', windowPtr, textSize);


%**************************************************************************
%       KEYBOARD
%**************************************************************************
% Enable unified mode of KbName, so KbName accepts identical key names on
% all operating systems:
KbName('UnifyKeyNames');
expdata.keySpace = KbName('space');
expdata.keyLeft = KbName('LeftArrow');
expdata.keyRight = KbName('RightArrow');
expdata.keys = [expdata.keySpace expdata.keyLeft expdata.keyRight];


%**************************************************************************
%       DISPLAY PARAMETERS
%**************************************************************************
expdata.windowPtr = windowPtr;
expdata.screenSize = screenSize;
expdata.screenCenter = screenCenter;
expdata.flipInterval = Screen('GetFlipInterval', windowPtr);
expdata.colorText = colorText;
expdata.colorBackground = colorBackground;
expdata.colorCircleDefault = colorCircleDefault;
expdata.colorCircleSubmit = colorCircleSubmit;
expdata.textSize = textSize;
expdata.textFont = textFont;


%**************************************************************************
%       START SCREEN
%**************************************************************************
DrawFormattedText(windowPtr, 'Experiment will begin shortly...', ...
    'center', 'center', colorText);
Screen(windowPtr, 'Flip');

end
