% Time Perception Task

%**************************************************************************
%       PTB INITIALIZATION/PARAMETER SETUP
%**************************************************************************
expdata = setup_experiment();

disp('Waiting for spacebar to continue...');
KbWaitForKeys(expdata.keySpace, Inf);


%**************************************************************************
%       CONTROL SESSION
%**************************************************************************
disp('Running control session...');
expdata.trialn = 1;
instruction = sprintf( ...
    [expdata.controlAction ' the area of the circle by a factor of ' ...
    '%0.1f.'], expdata.controlIncreaseFactor);
expdata = run_trial(expdata, instruction, expdata.controlStartRadius);


%**************************************************************************
%       DISPLAY REFERENCE DELAY/CIRCLE
%**************************************************************************
if expdata.showReferenceCircle
    disp('Displaying reference circle...');
    text = sprintf('This is a circle equivalent to %d day(s):', ...
        expdata.referenceCircleDelay);
    DrawFormattedText(expdata.windowPtr, text, 'center', 150, ...
        expdata.colorText);
    pos = [expdata.screenCenter(1) - expdata.referenceCircleRadius ...
        expdata.screenCenter(2) - expdata.referenceCircleRadius ...
        expdata.screenCenter(1) + expdata.referenceCircleRadius ...
        expdata.screenCenter(2) + expdata.referenceCircleRadius];
    Screen('FillOval', expdata.windowPtr, expdata.colorCircleDefault, pos);
    Screen(expdata.windowPtr, 'Flip');
    WaitSecs(expdata.timeDisplayReference);
end


%**************************************************************************
%       TESTING SESSIONS
%**************************************************************************
disp('Running testing sessions...');
idx = randperm(length(expdata.timeDelays));
delays = expdata.timeDelays(idx);

for trialn=2:expdata.ntrials+1
    expdata.trialn = trialn;
    instruction = sprintf('Draw a circle equivalent to %d day(s).', ...
        delays(trialn-1));
    expdata = run_trial(expdata, instruction, expdata.circleDefaultRadius);
end


%**************************************************************************
%       END EXPERIMENT
%**************************************************************************

end_experiment(expdata);
