function save_without_overwrite(filename, data)
% http://www.mathworks.com/help/techdoc/ref/sscanf.html
% http://www.mathworks.com/matlabcentral/answers/26095-autorename-if-saved-file-already-exist
[fPath, fName, fExt] = fileparts(filename);
if isempty(fExt)  % No '.mat' in filename
  fExt     = '.mat';
  filename = fullfile(fPath, [fName, fExt]);
end

if exist(filename, 'file')
  % Get number of files:
  % 'ver' for version
  fDir     = dir(fullfile(fPath, [fName, 'ver', '*', fExt]));
  fStr     = lower(sprintf('%s*', fDir.name));
  fNum     = sscanf(fStr, [fName, 'ver', '%d', fExt, '*']);
  if isempty(fNum),
      newNum = 2;
  else
    newNum   = max(fNum) + 1;
  end
  filename = fullfile(fPath, [fName, 'ver', sprintf('%d', newNum), fExt]);
end
save(filename, 'data');
