function end_experiment(expdata)

%**************************************************************************
%       SHUT DOWN EXPERIMENT
%**************************************************************************

% Save data.
expdata.end_time_serial = now;
expdata.end_time = datestr(expdata.end_time_serial,30);
save_without_overwrite([expdata.fname '_' expdata.end_time], expdata);

% Final screen.
DrawFormattedText(expdata.windowPtr, 'Thank you for participating.', ...
    'center', 'center', expdata.colorText);
Screen(expdata.windowPtr, 'Flip');
disp('End of experiment.');
disp('Waiting for spacebar to shut down display...');
KbWaitForKeys(KbName('space'), Inf);

% Clean up.
Screen('CloseAll');
Screen('Preference', 'SkipSyncTests', 0);

end
