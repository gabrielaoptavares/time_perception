function expdata = run_trial(expdata, instruction, startRadius)

WaitSecs(expdata.timeBetweenTrials);

DrawFormattedText(expdata.windowPtr, instruction, 'center', ...
    'center', expdata.colorText);
Screen(expdata.windowPtr, 'Flip');

WaitSecs(expdata.timeDisplayInstruction);

circleRadius = startRadius;
circlePos = update_circle_size(expdata, circleRadius);
Screen('FillOval', expdata.windowPtr, expdata.colorCircleDefault, ...
    circlePos);
Screen(expdata.windowPtr, 'Flip');

responseCount = 0;
startTime = GetSecs();

while true
    if GetSecs() - startTime > expdata.timeAdjustment
        disp('Maximum time allowed reached. No response recorded.');
        expdata.finalCircleRadius(expdata.trialn) = -1;
        break;
    end
     [parResponded, responseTime, reactionTime, keyCode] = ...
        KbWaitForKeys(expdata.keys, 1);
    if parResponded==1, % else no response
        responseCount = responseCount + 1;
        expdata.responseTime{expdata.trialn}(responseCount) = responseTime;
        expdata.reactionTime{expdata.trialn}(responseCount) = reactionTime;
        if keyCode(expdata.keyLeft)
            expdata.responseType{expdata.trialn}(responseCount) = -1;
            r = circleRadius - expdata.circleIncrementSize;
            if r > expdata.circleMinRadius
                circleRadius = r;
            end
        elseif keyCode(expdata.keyRight)
            expdata.responseType{expdata.trialn}(responseCount) = 1;
            r = circleRadius + expdata.circleIncrementSize;
            if r < expdata.circleMaxRadius
                circleRadius = r;
            end
        elseif keyCode(expdata.keySpace)
            fprintf('Response recorded. Final circle radius: %d.\n', ...
                circleRadius);
            expdata.responseType{expdata.trialn}(responseCount) = 0;
            expdata.finalCircleRadius(expdata.trialn) = circleRadius;
            Screen('FillOval', expdata.windowPtr, ...
                expdata.colorCircleSubmit, circlePos);
                Screen(expdata.windowPtr, 'Flip');
            break;
        end
        circlePos = update_circle_size(expdata, circleRadius);
        Screen('FillOval', expdata.windowPtr, ...
            expdata.colorCircleDefault, circlePos);
        Screen(expdata.windowPtr, 'Flip');
    end
end

WaitSecs(expdata.timeDisplayFinalCircle);

end

function circlePos = update_circle_size(expdata, radius)
    circlePos = [expdata.screenCenter(1) - radius ...
        expdata.screenCenter(2) - radius ...
        expdata.screenCenter(1) + radius ...
        expdata.screenCenter(2) + radius];
end